# Semantic-Role-Labeling-For-News

Semantic Role Labeling for News.

We provide two demo for the **Trump** news data: 

> - Coreference Resolution Demo
> - Semantic Role Labeling Demo



## File Structure

```Bash
Semantic_role_labeling_for_news/
\-----Data/
		\-----NER.npy
		\-----trump_porb
		\-----6_month/ # Not in the repo due to high volume
				\-----prod20181107_scheduled20181107t013500/
						\-----StepContent__nocontent_title_desc/
								\-----part000001
								\-----.....
						\-----StepLines__nocontent_title_desc/
								\-----part000001
								\-----.....
				\-----# Same for other months
\-----Result/
		\-----COREF/
				\-----Allennlp_Coref/  # Coref result. Default: on Trump data. Delete the folder when try to run on other dataset
				\-----Trump_data/
						\-----Allennlp_Coref/ # Coref result on Trump data
		\-----SRL/
				\-----Allennlp_Srl/ # Srl result. Default: on Trump data. Delete the folder when try to run on other dataset
				\-----Trump_data/
						\-----Allennlp_Srl/ # Srl result for Trump data
```



## Configuration

Please check the configuration files before running.

```Bash
/Source/COREF/coref_config.py
/Source/SRL/srl_config.py
```



## Coreference Resolution Demo

### > Play with Jupyter Notebook

#### 1. Enter the coreference resolution folder

```Bash
cd ./Source/COREF/
```

#### 2. Start jupyter notebook from command line, it will automatically open your browser 

```Bash
jupyter notebook
```

#### 3. Click coref_demo.ipynb

#### 4. Click run 



### > Play with terminal 

#### 1. Modify configuration

```bash
./Source/COREF/coref_config.py
```

#### 2. Start coref_visualization.py

```Bash
python3 coref_visualization.py 
```

Note that: if ./Result/COREF/Allennlp_Coref does not exist, it will automatically generate one and start predicting data using allennlp coreference resolution model. You can also do that by simply typing the command below from command line.

```Bash
python3 allen_coref_on_input_data.py
```



###### Note:

###### 1. correct files in COREF:

###### Since we modified the way to name the result file (from line id to article id), please label the correct result manually and store the correct article ids in correct_file_names (coref_config.py)

###### For now, the correct_file_names can be applied to the default Allennlp_Coref and ./Result/COREF/Trump_data/Allennlp_Coref/. You can enable read_from_correct_files by just a click.

###### 2. NER file

###### We Use NER file generated on Trump data from LDA by Di Qu.



## Semantic Role Labeling Demo

### > Play with Jupyter Notebook

#### 1. Enter the semantic role labeling folder

```Bash
cd ./Source/SRL/
```

#### 2. Start jupyter notebook from command line, it will automatically open your browser 

```Bash
jupyter notebook
```

#### 3. Click srl_demo.ipynb

#### 4. Click run 



### > Play with terminal 

#### 1. Modify configuration

```bash
./Source/SRL/srl_config.py
```

#### 2. Start srl_visualization.py

```Bash
python3 srl_visualization.py 
```



Note that: Semantic role labeling may depend on the result of coreference resolution if the flag enable_coreference_resolution is set to true.

1. if ./Result/SRL/Allennlp_Srl does not exist, it will automatically generate one and start predicting data using allennlo semantic role labeling mode. 
2. For coreference resolution, as mentioned above, semantic role labeling may rely on its result. if ./Result/COREF/Allennlp_Coref does not exist, semantic role labeling will also automatically generate one and start predicting data using allennlp coreference resolution model. You can also do that by simply typing the command below from command line.

```Bash
python3 allen_coref_on_input_data.py
```


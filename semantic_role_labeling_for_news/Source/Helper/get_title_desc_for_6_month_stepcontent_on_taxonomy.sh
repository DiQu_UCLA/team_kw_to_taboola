#!/usr/bin/env bash

TOPIC=${1?Error: no topic given}

python3 get_title_desc_for_given_taxonomy.py -t "$TOPIC" -f ../../Data/6_month/prod20181107_scheduled20181107t013500/StepContent__nocontent_title_desc/

python3 get_title_desc_for_given_taxonomy.py -t "$TOPIC" -f ../../Data/6_month/prod20181207_scheduled20181207t013500/StepContent__nocontent_title_desc/

python3 get_title_desc_for_given_taxonomy.py -t "$TOPIC" -f ../../Data/6_month/prod20190106_scheduled20190106t013500/StepContent__nocontent_title_desc/

python3 get_title_desc_for_given_taxonomy.py -t "$TOPIC" -f ../../Data/6_month/prod20190205_scheduled20190205t013500/StepContent__nocontent_title_desc/

python3 get_title_desc_for_given_taxonomy.py -t "$TOPIC" -f ../../Data/6_month/prod20190307_scheduled20190307t013500/StepContent__nocontent_title_desc/

python3 get_title_desc_for_given_taxonomy.py -t "$TOPIC" -f ../../Data/6_month/prod20190406_scheduled20190406t013500/StepContent__nocontent_title_desc/

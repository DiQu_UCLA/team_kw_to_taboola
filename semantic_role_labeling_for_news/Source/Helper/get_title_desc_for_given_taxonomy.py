import json
import os
import getopt
import sys

from tqdm import tqdm

interested_taxonomy = str()
input_folder = str()
output_folder = str()

# examples
# interested_taxonomy = '/sports/baseball'
# months = ['prod20181107_scheduled20181107t013500', 'prod20181207_scheduled20181207t013500', 'prod20190106_scheduled20190106t013500',
# 'prod20190205_scheduled20190205t013500', 'prod20190307_scheduled20190307t013500', 'prod20190406_scheduled20190406t013500']
# input_folder = '../../Data/6_month/prod20190406_scheduled20190406t013500/StepContent__nocontent_title_desc/'
# output_folder = '../../Result/TopicInfo/baseball/prod20190406_scheduled20190406t013500/'
max_file_length = 5000

class data_entry:
    def __init__(self, url, title_desc, title):
        self.url = url
        self.title_desc = title_desc
        self.title = title

def dump_result_to_file(output_folder, input_data_entries, file_id):
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)
    new_file_path = output_folder + str(file_id)
    if not os.path.exists(new_file_path):
        open(new_file_path, 'a').close()
    with open(new_file_path, 'w') as f:
        for de_id in range(len(input_data_entries)):
            de = input_data_entries[de_id]
            overall_id = (file_id-1)*5000+de_id
            f.write(str(overall_id) + '\t' + de['url'] + '\t' + de['title_desc'] + '\t' + de['title'])
            if de_id < len(input_data_entries)-1:
                f.write('\n')

    return list(), file_id+1

def get_data_entry_info_from_json_data(input_folder, output_folder):
    input_data_entries = list()
    sorted_file_names = list()
    file_id = 1
    for root, dirs, files in os.walk(input_folder, topdown=False):
        for name in files:
            if '.' in name:
                continue
            sorted_file_names.append(name)
    sorted_file_names.sort()
    pbar = tqdm(range(len(sorted_file_names)))
    for name in sorted_file_names:
        with open(input_folder + name, 'r') as f:
            # print('loading file from : '+input_folder + name)
            pbar.update(1)
            for line in f:
                data = json.loads(line)
                if 'title_desc' not in data or data['title_desc'] == None:
                    continue
                if 'url' not in data or data['url'] == None:
                    continue
                if 'title' not in data or data['title'] == None:
                    continue
                if data['taxonomy'] == interested_taxonomy:
                    de = data_entry(data['url'], data['title_desc'], data['title'])
                    input_data_entries.append(data)
                    if len(input_data_entries) >= max_file_length:
                        input_data_entries, file_id = dump_result_to_file(output_folder, input_data_entries, file_id)

    input_data_entries, file_id = dump_result_to_file(output_folder, input_data_entries, file_id)
    pbar.close()

def get_output_folder(taxonomy, input_folder):
    output_folder = str()
    # get topic
    categories = taxonomy.split('/')
    topic = categories[-1]

    tokens = input_folder.split('/')
    for token in tokens:
        if token.startswith('prod') and 'scheduled' in token:
            output_folder = '../../Result/TopicInfo/' + topic + '/' + token + '/'
            break
    return output_folder

def get_input_metadata(argv):
    global interested_taxonomy, input_folder, output_folder

    try:
        opts, args = getopt.getopt(argv, "ht:f:", ["help", "topic=", "folder="])
    except getopt.GetoptError:
        print('Error: get_title_desc_for_given_taxonomy.py -t <topic> -f <folder_path>')
        print('or: get_title_desc_for_given_taxonomy.py --topic=<topic> --folder=<folder_path>')
        sys.exit(2)

    for opt, arg in opts:
        if opt in ("-h", "--help"):
            print('get_title_desc_for_given_taxonomy.py -t <topic> -f <folder_path>')
            print('or: get_title_desc_for_given_taxonomy.py --topic=<topic> --folder=<folder_path>')
            sys.exit()
        elif opt in ("-f", "--folder"):
            if arg.endswith('/'):
                input_folder = arg
            else:
                input_folder = arg + '/'
        elif opt in ("-t", "--topic"):
            interested_taxonomy = arg

    output_folder = get_output_folder(interested_taxonomy, input_folder)

    print("topic: " + interested_taxonomy)
    print("input_folder: " + input_folder)
    print("output_folder: " + output_folder)

if __name__ == '__main__':
    get_input_metadata(sys.argv[1:])
    get_data_entry_info_from_json_data(input_folder, output_folder)
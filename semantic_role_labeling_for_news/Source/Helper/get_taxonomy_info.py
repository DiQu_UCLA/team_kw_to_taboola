import json
import os
import operator
import getopt
import sys

from tqdm import tqdm
from collections import OrderedDict

input_folder = str()
# input_folder = '../../Data/6_month/prod20190406_scheduled20190406t013500/StepContent__nocontent_title_desc/'
first_level_taxonomy_file_path = '../../Result/TopicInfo/first_level_taxonomy_mappings'
taxonomy_file_path = '../../Result/TopicInfo/taxonomy_mappings'

def load_mappings(first_level_taxonomy_file_path, taxonomy_file_path):
    first_level_taxonomy_mappings = dict()
    taxonomy_mappings = dict()
    if os.path.exists(first_level_taxonomy_file_path):
        with open(first_level_taxonomy_file_path, 'r') as f:
            first_level_taxonomy_mappings = json.load(f)
            print("Detected existing first_level_taxonomy mapping. Load from " + first_level_taxonomy_file_path)
    if os.path.exists(taxonomy_file_path):
        with open(taxonomy_file_path, 'r') as f:
            taxonomy_mappings = json.load(f)
            print("Detected existing taxonomy mapping. Load from " + taxonomy_file_path)
    return first_level_taxonomy_mappings, taxonomy_mappings 

def dump_sorted_data(output_file, data):
    sorted_data = OrderedDict(sorted(data.items(), key=lambda x: x[1], reverse=True))
    with open(output_file, 'w') as outfile:  
        json.dump(sorted_data, outfile)

def get_json_data(input_folder, first_level_taxonomy_mappings, taxonomy_mappings):
    input_data = list()
    sorted_file_names = list()
    for root, dirs, files in os.walk(input_folder, topdown=False):
        for name in files:
            if '.' in name:
                continue
            sorted_file_names.append(name)
    sorted_file_names.sort()
    pbar = tqdm(range(len(sorted_file_names)))
    for name in sorted_file_names:
        with open(input_folder + name, 'r') as f:
            pbar.update(1)
            for line in f:
                data = json.loads(line)
                if data["first_level_taxonomy"] not in first_level_taxonomy_mappings:
                    first_level_taxonomy_mappings[data["first_level_taxonomy"]] = 1
                else:
                    first_level_taxonomy_mappings[data["first_level_taxonomy"]] += 1
                if data["taxonomy"] not in taxonomy_mappings:
                    taxonomy_mappings[data["taxonomy"]] = 1
                else:
                    taxonomy_mappings[data["taxonomy"]] += 1
    pbar.close()

    dump_sorted_data(first_level_taxonomy_file_path, first_level_taxonomy_mappings)
    dump_sorted_data(taxonomy_file_path, taxonomy_mappings)
    return input_data

def get_input_folder_name(argv):
    global input_folder

    try:
        opts, args = getopt.getopt(argv, "hf:", ["help", "folder="])
    except getopt.GetoptError:
        print('Error: get_taxonomy_info.py -f <folder_path>')
        print('or: get_taxonomy_info.py --folder=<folder_path>')
        sys.exit(2)

    for opt, arg in opts:
        if opt in ("-h", "--help"):
            print('get_taxonomy_info.py -f <folder_path>')
            print('or: get_taxonomy_info.py --folder=<folder_path>')
            sys.exit()
        elif opt in ("-f", "--folder"):
            if arg.endswith('/'):
                input_folder = arg
            else:
                input_folder = arg + '/'

if __name__ == '__main__':
    get_input_folder_name(sys.argv[1:])
    print("input folder = " + input_folder)
    first_level_taxonomy_mappings, taxonomy_mappings = load_mappings(first_level_taxonomy_file_path, taxonomy_file_path)
    input_data = get_json_data(input_folder, first_level_taxonomy_mappings, taxonomy_mappings)